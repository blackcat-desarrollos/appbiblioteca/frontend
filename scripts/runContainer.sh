py -m pip freeze > config/requirements.txt
cp ./Docker/* .
docker build -t AteneoBiblioteca .
docker run -d -p 5000:5000 AteneoBiblioteca