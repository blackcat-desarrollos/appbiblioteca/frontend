#!/usr/bin/env sh
DIR=$(pwd)
export FLASK_ENV=development
export FLASK_DEBUG=1
export FLASK_APP="$DIR/WEB.py"
echo "Running $FLASK_APP..."
cd $DIR/static/scripts
curl -X POST -s --data-urlencode 'input@index.js' https://javascript-minifier.com/raw >index.min.js
if [ ! -z $(grep "Error" index.min.js) ]; then exit; fi
cd $DIR
py -m flask run --reload --port 5001
