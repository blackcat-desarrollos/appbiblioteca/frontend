#!/usr/bin/env sh
export FLASK_ENV=development
export FLASK_DEBUG=1
export FLASK_APP="$(pwd)/example.py"
echo "Running $FLASK_APP..."
py -m flask run --reload
