import { fetchURL } from './modules/service.mjs';

function bookshelfLink(tituloColumna, field, labelField) {
  return {
    title: tituloColumna, field: field, formatter: "link", formatterParams: {
      labelField: labelField,
      urlPrefix: "http://localhost:5001/bookshelf/",
      target: "_blank",
    }
  }
}

fetchURL("http://localhost:5000/rest/biblioteca").then((res) => {
    console.log(res);
    GenerateTable("bookshelf-table", [
      bookshelfLink("Nombre", "Nombre", "Nombre"),
      { title: "Autores", field: "Autores", sorter: "string", width: 200 },
    ], res['data'])
  })