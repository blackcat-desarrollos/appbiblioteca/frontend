

function GenerateTable(tableId, columnArray, data) {
  return new Tabulator("#" + tableId, {
    maxHeight: "100%",
    minHeight: 400,
    layout: "fitDataStretch",
    columns: columnArray,
  }).setData(data);
}

$(function () {
    // Que hacer cuando el documento esté ready
});

