import configparser
import json
import requests
from flask import render_template
from bson import json_util

ParsedConfig = configparser.ConfigParser()
ParsedReader = ParsedConfig.read('config/properties.ini')


def BSONtoJSON(bson):
    return json.loads(json_util.dumps(bson))

def getBook(bid=None, apiUrl=ParsedConfig.get('API', 'URL')):
    if bid is not None:
        return requests.request('GET', apiUrl)

def getBooks(apiUrl=ParsedConfig.get('API', 'URL')):
    return requests.get(apiUrl)

def ErrorPage(code, msg):
    return render_template("error.html", error_code=code, error_msg=msg)