import logging
import requests
from flask import Flask, render_template
from classes.Utils import *

# from classes.BookShelf import MakeTable

app = Flask(__name__)
logging.basicConfig(level=logging.INFO)

AcceptedMethods = ['GET']


# req = getFromApi().json()
# table = MakeTable(req["docs"])
# req['collection']

@app.route("/", methods=AcceptedMethods)
def WEB():
    try:
        return render_template("home.html")
    except Exception as err:
        return ErrorPage(404, f"{err}")


@app.route("/bookshelf", methods=AcceptedMethods)
def Bookshelf():
    return render_template("bookshelf.html")


@app.route("/bookshelf/<bookID>", methods=AcceptedMethods)
def Book(bid):
    req = getBook(bid).json()
    return render_template('bookshelf.html')


if __name__ == '__main__':
    configurations = ""
    for sect in ParsedConfig.sections():
        configurations = configurations + 'Section: {}'.format(sect)
        for k, v in ParsedConfig.items(sect):
            configurations = configurations + ' {} = {} \n'.format(k, v)

    logging.log(configurations)
    app.run(port=ParsedConfig.get('commons', 'app_port'))
